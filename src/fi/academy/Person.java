package fi.academy;

public class Person {

    String address;
    String city;
    String phoneNumber;
    String name;
    int age;
    

    public Person(String address, String city, String phoneNumber, String name, int age) {
        this.address = address;
        this.city = city;
        this.phoneNumber = phoneNumber;
        this.name = name;
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    public String getName(){
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge (){
        return age;

    }

    public void setAge (int age){
        this.age=age;
    }

    @Override
    public String toString() {
        return "Person{" +  "Name: " + name + '\'' + " age: " + age + '\'' +
                "address: " + address + '\'' +
                ", city: " + city + '\'' +
                ", phoneNumber: " + phoneNumber + '\'' +
                '}';
    }
}

